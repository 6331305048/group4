import Vue from "vue";
import VueRouter from 'vue-router';

import Vuesax from 'vuesax';
import 'vuesax/dist/vuesax.css';
Vue.use(Vuesax)


import App from "./App.vue";

import Login from "./views/Login.vue";
import Home from "./views/Home.vue";
import Member from "./views/Member.vue";

Vue.use(VueRouter);
const routes = [
  {
    name: "Login",
    path: "/",
    component: Login
  },
  {
    name: "Home",
    path: "/Homepage",
    component: Home
  },
  {
    name: "Member",
    path: "/Member.vue",
    component: Member
  }
];
const router = new VueRouter({ mode: "history", routes: routes });
Vue.config.productionTip = false;
new Vue({
  render: h => h(App),
  router
}).$mount("#app");